# ckanext-dathere_theme2
=============
ckanext-dathere_theme2
=============

------------
Requirements
------------

Compatible with CKAN 2.7+

Install the ckanext-showcase extension


------------
Development Installation
------------

To install ckanext-dathere_theme2:

1. Activate your CKAN virtual environment, for example::

     . /usr/lib/ckan/default/bin/activate

2. Download the ckanext-dathere_theme2 github repository::

     cd /usr/lib/ckan/default/src
     git clone https://gitlab.com/dathere/ckanext-dathere_theme2.git

3. Install the extension into your virtual environment::

     cd ckanext-dathere_theme2
     python setup.py develop

4. Add ``dathere_theme2`` to the ``ckan.plugins`` setting in your CKAN
   config file (by default the config file is located at
   ``/etc/ckan/default/production.ini``).

5. Restart CKAN. For example if you've deployed CKAN with Apache on Ubuntu::

     sudo service apache2 reload


---------------
Config Settings
---------------

Document any optional config settings here. For example::

    # The minimum number of hours to wait before re-checking a resource
    # (optional, default: 24).
    ckanext.dathere_theme2.some_setting = some_default_value
